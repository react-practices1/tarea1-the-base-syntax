import React from 'react';
import './UserOutput.css'

const useroutput = (props) => {
    return( 
        <div className='UserOutput'>
            <p>Some random useroutput!</p>
            <p>Line two of the text!</p>
            <p>Name Out: {props.userName}</p>
        </div> 
    );
}

export default useroutput;